package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

var identity = flag.String("identity", "", "specify an identity that owns the keys to sign data with")
var source = flag.String("source", "", "specify an address or an alias to be the source of money")
var contract = flag.String("contract", "", "specify an alias or an address of the oracle contract")
var debug = flag.Bool("debug", false, "print out more information")

const endpoint = "https://www.bitstamp.net/api/v2/ticker_hour/btcusd/"

//{"high": "6351.00", "last": "6331.00", "timestamp": "1509458347", "bid": "6327.03", "vwap": "6277.16", "volume": "1183.21242286", "low": "6193.07", "ask": "6330.98", "open": "6200.00"}

//TickerHour holds the results of calls to ticker endpoint of Bistamp API
type TickerHour struct {
	High      float32 `json:"high,string"`
	Last      float32 `json:"last,string"`
	Timestamp int     `json:"timestamp,string"`
	Bid       float32 `json:"bid,string"`
	VWap      float32 `json:"vwap,string"`
	Volume    float32 `json:"volume,string"`
	Low       float32 `json:"low,string"`
	Ask       float32 `json:"ask,string"`
	Open      float32 `json:"open,string"`
}

//ALPHANET_EMACS has to be set when running alphanet.sh from a program, otherwise it won't be able to detect the running node.

//The program consists of these three parts: a main goroutine that handles set up and errors, a dispatcher goroutine that creates update goroutines in 1 hour intervals, and update goroutines that take care of getting data from Bitstamp API and pushing it to the oracle contract.
//When an update goroutine encounters an error, it sends the error using the channel ch to the main goroutine, which stops the timer, sends a shutdown signal to the dispatcher, prints the error and exits.

func main() {
	flag.Parse()
	if *debug {
		fmt.Println("identity:", *identity)
		fmt.Println("source:", *source)
		fmt.Println("contract:", *contract)
		wd, _ := os.Getwd()
		fmt.Println("getwd", wd)
		fmt.Println("getenv:", os.Getenv("PATH"))
	}

	//check that the node is running
	cmd := exec.Command("./alphanet.sh", "head")
	cmd.Env = append(cmd.Env, "ALPHANET_EMACS=true")
	b, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println("error:", string(b))
		log.Fatal(err)
	}
	if strings.Contains(string(b), "Node is not running!") {
		fmt.Println("Node is not running!")
		fmt.Println("Start the node first with:")
		fmt.Println("\t./alphanet.sh start")
	}

	if *debug {
		fmt.Println(string(b))
	}

	ch := make(chan error)
	shutdown := make(chan bool)
	ticker := time.NewTicker(time.Hour)
	fmt.Printf("ticker: %#v\n", ticker)
	go dispatchUpdates(shutdown, ticker.C, ch)
	for {
		select {
		case err := <-ch:
			ticker.Stop()
			shutdown <- true
			log.Fatal(err)
		}
	}
}

func getData() (TickerHour, error) {
	var th TickerHour
	resp, err := http.Get(endpoint)
	if err != nil {
		return th, fmt.Errorf("querying http endpoint: %v", err)
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return th, fmt.Errorf("reading response: %v", err)
	}
	err = json.Unmarshal(b, &th)
	if err != nil {
		return th, fmt.Errorf("unmarshaling json: %v", err)
	}
	return th, err
}

func updateData(ch chan<- error) {
	th, err := getData()
	if err != nil {
		ch <- fmt.Errorf("getting data: %v", err)
		return
	}
	high := int(th.High * 100)
	counter, err := getCounter(*contract)
	if err != nil {
		ch <- fmt.Errorf("getting counter: %v", err)
		return
	}
	signature, err := signData(high, counter, *identity)
	if err != nil {
		ch <- fmt.Errorf("signing data: %v", err)
		return
	}
	err = transferData(high, counter, signature, *source, *contract)
	if err != nil {
		ch <- fmt.Errorf("transferring data: %v", err)
		return
	}
}

func getCounter(contract string) (int, error) {
	c := exec.Command("./alphanet.sh", "client", "get", "storage", "for", contract)
	c.Env = append(c.Env, "ALPHANET_EMACS=true")
	b, err := c.CombinedOutput()
	if err != nil {
		fmt.Println("error:", string(b))
		return 0, fmt.Errorf("running commands to get storage: %v", err)
	}
	if *debug {
		fmt.Println(string(b))
	}
	words := strings.Fields(string(b))
	w := strings.TrimSuffix(words[3], ")")
	counter, err := strconv.Atoi(w)
	if err != nil {
		fmt.Println(w)
		return 0, fmt.Errorf("converting output to int: %v", err)
	}
	return counter, nil
}

func signData(data, counter int, identity string) (string, error) {
	prepared := fmt.Sprintf("(Pair %d %d)", data, counter)
	c := exec.Command("./alphanet.sh", "client", "hash", "and", "sign", "data", prepared, "for", identity)
	c.Env = append(c.Env, "ALPHANET_EMACS=true")
	b, err := c.CombinedOutput()
	if err != nil {
		fmt.Println("error:", string(b))
		return "", fmt.Errorf("running commands to sign: %v", err)
	}
	if *debug {
		fmt.Println(string(b))
	}
	lines := strings.Split(string(b), "\n")
	for _, l := range lines {
		l := strings.TrimSpace(l)
		if strings.HasPrefix(l, "Signature:") {
			words := strings.Split(l, " ")
			signature := strings.Trim(words[1], "\"")
			return signature, nil
		}
	}
	return "", fmt.Errorf("parsing output: signature not found")
}

func transferData(data, counter int, signature, source, contract string) error {
	arg := fmt.Sprintf("(Some (Pair \"%s\" (Pair %d %d)))", signature, data, counter)
	c := exec.Command("./alphanet.sh", "client", "transfer", "0", "from", source, "to", contract, "-arg", arg)
	c.Env = append(c.Env, "ALPHANET_EMACS=true")
	b, err := c.CombinedOutput()
	if err != nil {
		fmt.Println("error:", string(b))
		return fmt.Errorf("running commands to transfer: %v", err)
	}
	if *debug {
		fmt.Println(string(b))
	}
	return nil
}

func dispatchUpdates(shutdown <-chan bool, tc <-chan time.Time, ch chan<- error) {
	fmt.Println("dispatching first update")
	go updateData(ch)
	for {
		select {
		case _ = <-shutdown:
			return
		case _ = <-tc:
			fmt.Println("dispatching subsequent update")
			go updateData(ch)
		}
	}
}
